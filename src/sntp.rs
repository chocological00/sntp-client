use std::
{
	net::Ipv4Addr, 
	string::FromUtf8Error,
};

use fixed::
{
	FixedI32, 
	FixedU32,
	FixedU64,
	types::extra::{U16, U32}
};
use num_enum::TryFromPrimitive;

use crate::utils;

#[derive(Debug)]
pub struct NtpPacket
{
	pub li: LeapIndicator, // first 2 bits, server only
	pub vn: u8, // next 3 bits, latest is 4
	pub mode: ProtocolMode, // next 3 bits
	pub stratum: u8, // server only, 0 = death, 1 = primary (i.e. radio clock), 2-15 = secondary (i.e. synchronized by other (S)NTP), 16-255 = reserved
	pub poll: u8, // server only, only 4-17 valid
	pub precision: i8, // server only, precision of clock in seconds in exponent of 2 notation
	pub root_delay_sec: FixedI32<U16>, // server only, total RTT to primary reference in seconds
	pub root_dispersion: FixedU32<U16>, // server only, maximum error in seconds
	pub reference_identifier: ReferenceIdentifier, // server only, info about clock source
	pub reference_timestamp: FixedU64<U32>, // timestamp, when system clock was last set/corrected
	pub originate_timestamp: FixedU64<U32>, // timestamp, when request was sent from client to server
	pub receive_timestamp: FixedU64<U32>, // timestamp, when request was received by receiver
	pub transmit_timestamp: FixedU64<U32> // timestamp, when reply was sent by sender
}

#[derive(Debug, TryFromPrimitive, Clone, Copy, PartialEq)]
#[repr(u8)]
pub enum LeapIndicator
{
	NoWarning,
	LastMinute61,
	LastMinute59,
	Alarm // clock not synchronized
}

#[derive(Debug, TryFromPrimitive, Clone, Copy, PartialEq)]
#[repr(u8)]
pub enum ProtocolMode
{
	Reserved, // 0
	SymmetricActive,
	SymmetricPassive,
	Client, // 3, client request for unicast
	Server, // 4, server reply for unicast
	Broadcast,
	ReservedControlMessage, // 6
	ReservedPrivateUse // 7
}

#[derive(Debug)]
pub enum ReferenceIdentifier
{
	ReferenceSource(String), // 4 byte C string, only for stratum 0/1
	Ipv4(Ipv4Addr), // 4 byte IP address, only for IPv4 servers
	MD5([u8; 4]) // first 32bit of MD5 digest of either IPv6 address or NSAP address
}

enum ReferenceIdentifierEncodeError
{
	ReferenceSourceTooLong
}

impl ReferenceIdentifier
{
	fn encode(&self) -> Result<[u8; 4], ReferenceIdentifierEncodeError>
	{
		let mut bytes = [0u8; 4];
		match self
		{
			ReferenceIdentifier::ReferenceSource(s) =>
			{
				if s.len() > 4
				{
					return Err(ReferenceIdentifierEncodeError::ReferenceSourceTooLong);
				}
				for (i, c) in s.as_bytes().iter().enumerate()
				{
					bytes[i] = *c;
				}
			},
			ReferenceIdentifier::Ipv4(ip) => bytes.copy_from_slice(&ip.octets()), // this always return big endian order, so no need to flip
			ReferenceIdentifier::MD5(md5) => bytes.copy_from_slice(md5)
		}
		Ok(bytes)
	}
}

pub enum DecodeError
{
	InvalidPacketLength,
	InvalidUtf8(FromUtf8Error)
}

impl NtpPacket
{
	pub fn decode(bytes: &[u8], ipv4: bool) -> Result<Self, DecodeError>
	{
		if bytes.len() < 48
		{
			return Err(DecodeError::InvalidPacketLength);
		}

		let li = LeapIndicator::try_from(bytes[0] >> 6).unwrap(); // safe because >>6 caps the range to 0-4
		let vn = bytes[0] << 2 >> 5;
		let mode = ProtocolMode::try_from(bytes[0] << 5 >> 5).unwrap(); // safe because resulting bits are of range 0-7
		let stratum = bytes[1];
		let poll = bytes[2];
		let precision = bytes[3] as i8;
		let root_delay_sec = FixedI32::from_be_bytes(bytes[4..8].try_into().unwrap());
		let root_dispersion = FixedU32::from_be_bytes(bytes[8..12].try_into().unwrap());
		let reference_identifier = if stratum < 2
		{
			let nul_range_end = bytes[12..16].iter().position(|&x| x == 0).unwrap_or(4) + 12;
			ReferenceIdentifier::ReferenceSource(String::from_utf8(bytes[12..nul_range_end].to_vec()).map_err(DecodeError::InvalidUtf8)?)
		}
		else if ipv4
		{
			ReferenceIdentifier::Ipv4(Ipv4Addr::from(u32::from_be_bytes(bytes[12..16].try_into().unwrap()))) // this unwrap also safe
		}
		else
		{
			ReferenceIdentifier::MD5(bytes[12..16].try_into().unwrap())
		};
		let reference_timestamp = FixedU64::from_be_bytes(bytes[16..24].try_into().unwrap());
		let originate_timestamp = FixedU64::from_be_bytes(bytes[24..32].try_into().unwrap());
		let receive_timestamp = FixedU64::from_be_bytes(bytes[32..40].try_into().unwrap());
		let transmit_timestamp = FixedU64::from_be_bytes(bytes[40..48].try_into().unwrap());
		Ok(
			Self
			{
				li,
				vn,
				mode,
				stratum,
				poll,
				precision,
				root_delay_sec,
				root_dispersion,
				reference_identifier,
				reference_timestamp,
				originate_timestamp,
				receive_timestamp,
				transmit_timestamp
			}
		)
	}
}

#[derive(Debug)]
pub enum EncodeError
{
	InvalidVersion,
	ReferenceSourceTooLong
}

impl NtpPacket
{
	pub fn encode(&self) -> Result<Vec<u8>, EncodeError>
	{
		if self.vn > 8
		{
			return Err(EncodeError::InvalidVersion);
		}
		let mut bytes = vec![
			(self.li as u8) << 6 | self.vn << 3 | self.mode as u8,
			self.stratum,
			self.poll,
			self.precision as u8,
		];
		bytes.extend_from_slice(&self.root_delay_sec.to_be_bytes());
		bytes.extend_from_slice(&self.root_dispersion.to_be_bytes());
		bytes.extend_from_slice(&self.reference_identifier.encode().map_err(|_| EncodeError::ReferenceSourceTooLong)?);
		bytes.extend_from_slice(&self.reference_timestamp.to_be_bytes());
		bytes.extend_from_slice(&self.originate_timestamp.to_be_bytes());
		bytes.extend_from_slice(&self.receive_timestamp.to_be_bytes());
		bytes.extend_from_slice(&self.transmit_timestamp.to_be_bytes());
		Ok(bytes)
	}

	pub fn gen_request() -> (Vec<u8>, FixedU64<U32>)
	{
		let transmit_timestamp = utils::get_current_ntp_timestamp();

		let pkt = Self
		{
			li: LeapIndicator::NoWarning,
			vn: 4,
			mode: ProtocolMode::Client,
			stratum: 0,
			poll: 0,
			precision: 0,
			root_delay_sec: FixedI32::from_bits(0),
			root_dispersion: FixedU32::from_bits(0),
			reference_identifier: ReferenceIdentifier::MD5([0u8; 4]),
			reference_timestamp: FixedU64::from_bits(0),
			originate_timestamp: FixedU64::from_bits(0),
			receive_timestamp: FixedU64::from_bits(0),
			transmit_timestamp
		}.encode().unwrap(); // this unwrap also safe because both vn and reference_id is hardcoded

		(pkt, transmit_timestamp)
	}
}