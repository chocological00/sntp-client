use std::time::{SystemTime, Duration, UNIX_EPOCH};

use fixed::{FixedU64, types::extra::U32};

const NTP_EPOCH_OFFSET_SECS: u64 = 2_208_988_800; // UNIX_EPOCH - NTP_ERA_ZERO (as specified in RFC5905)

pub fn get_current_ntp_timestamp() -> fixed::FixedU64<U32>
{
	let ntp_timestamp = SystemTime::now().duration_since(UNIX_EPOCH).unwrap() // safe, time can't go backwards
		.saturating_add(Duration::from_secs(NTP_EPOCH_OFFSET_SECS)).as_secs_f64();
	FixedU64::from_num(ntp_timestamp)
}

pub fn mean(v: &[f64]) -> f64
{
	v.iter().sum::<f64>() / v.len() as f64
}

pub fn stdev(v: &[f64], mean: f64) -> f64
{
	let mut sum = 0.0;
	for i in v { sum += (i - mean).powi(2); }
	(sum / (v.len() - 1) as f64).sqrt()
}