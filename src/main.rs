// for SNTP specs, 
// see [RFC4330](https://tools.ietf.org/html/rfc4330) (deprecated, but has more details)
// and [section 14 of RFC5905](https://datatracker.ietf.org/doc/html/rfc5905#section-14)
use std::
{
	sync::Arc, 
	collections::{HashMap, HashSet}, 
	time::Duration
};

use tokio::{
	sync::RwLock,
	time::timeout,
	net::{UdpSocket, lookup_host}
};

mod sntp;
mod utils;

use sntp::NtpPacket;

#[tokio::main]
async fn main() 
{
	// env vars
	let mut remote_addr = std::env::var("SERVER").expect("SERVER not set");
	let num = std::env::var("N").expect("N not set");
	let to = std::env::var("TIMEOUT").expect("TIMEOUT not set");
	let verbose = std::env::var("VERBOSE").is_ok();

	// parse env vars
	if !remote_addr.contains(':')
	{
		remote_addr.push_str(":123");
	}
	let num = num.parse::<usize>().expect("N must be a number");
	let to = to.parse::<u64>().expect("TIMEOUT must be a number");

	// get server's ip address
	let remote_addr = lookup_host(&remote_addr).await.expect("DNS Lookup error!")
		.next().expect("DNS lookup returned no results!");
	let ipv4 = remote_addr.is_ipv4();

	// bind to 0.0.0.0:0, or "any port available on this computer"
	let sock = Arc::new(UdpSocket::bind("0.0.0.0:0").await.expect("Could not create UDP socket!"));
	// connect ensures that we're filtering udp packets from senders that aren't our server
	sock.connect(remote_addr).await.expect("Could not connect to remote address!");


	let sent_packets = Arc::new(RwLock::new(HashSet::new()));

	// receive in a separate thread so we can measure latency accurately (without being blocked by send)
	// spawn receiver first before sending, so packets won't spend time in buffer
	let s = sock.clone();
	let ss = sent_packets.clone();
	let recv_thread = tokio::spawn(
		async move
		{
			let sock = s;
			let sent_packets = ss;
			let mut recv_pkts = 0;
			let mut map = HashMap::new();

			// receive until buffer runs out / we received enough packets
			loop
			{
				let mut buf = [0u8; 48]; // if packet is larger than 48, rest is thrown away
				if recv_pkts >= num { break; }
				let t4;
				let res = if let Ok(r) = timeout(Duration::from_secs(to), sock.recv(&mut buf)).await
				{
					t4 = utils::get_current_ntp_timestamp(); // gen ASAP
					if let Ok(read) = r
					{
						if read < 48 { continue; } // if packet is too small
					}
					else { continue; }
					if let Ok(r) = NtpPacket::decode(&buf, ipv4) { r }
					else { continue; }
				}
				else { break; }; // break on timeout

				recv_pkts += 1;
				map.insert(t4, res);
			}

			// eprintln!("{:?}", map);

			let mut valid_pkts = 0;
			let mut sent_packets = sent_packets.write().await; // rwlock unwrap is safe unless unsafe is used
			let mut delays = Vec::new();
			let mut offsets = Vec::new();

			// for each packet we received
			for (t4, res) in map
			{
				// sanity checks
				// if res.li == sntp::LeapIndicator::NoWarning { continue; } // TODO: check
				// if res.vn != 4 { continue; }
				if res.mode != sntp::ProtocolMode::Server { continue; }
				if res.stratum == 0 || res.stratum > 16 { continue; }
				if res.transmit_timestamp == 0 { continue; }
				if res.root_delay_sec < 0 || res.root_delay_sec > 1 { continue; }
				if res.root_dispersion > 1 { continue; }

				// t1, t2, t3, t4 defined as spec in RFC4330 section 5
				let t1 = res.originate_timestamp;
				if !sent_packets.contains(&t1) { continue; }
				else { sent_packets.remove(&t1); }
				valid_pkts += 1;
				let t2 = res.receive_timestamp;
				let t3 = res.transmit_timestamp;
				let rt_delay = (t4 - t1) - (t3 - t2);
				let offset = ((t2 - t1) + (t3 - t4)) / 2;
				delays.push(rt_delay.to_num::<f64>());
				offsets.push(offset.to_num::<f64>());
				if verbose
				{
					println!("delay: {}, offset: {}", rt_delay, offset);
				}
			}

			println!("{} packets sent, {} packets received, {} packets timed out, {} packets failed assertion", num, valid_pkts, num - recv_pkts, recv_pkts - valid_pkts);
			println!("Delay - mean: {}, stdev: {}", utils::mean(&delays), utils::stdev(&delays, utils::mean(&delays)));
			println!("Offset - mean: {}, stdev: {}", utils::mean(&offsets), utils::stdev(&offsets, utils::mean(&offsets)));
		}
	);

	// send all at once
	for _ in 0..num
	{
		let (request, sent_timestamp) = NtpPacket::gen_request();
		sock.send(&request).await.expect("Could not send request!");
		sent_packets.write().await.insert(sent_timestamp);
	}

	// wait for our receiver to finish
	let _ = recv_thread.await;
}
